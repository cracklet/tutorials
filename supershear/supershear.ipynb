{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# cRacklet Tutorial - Supershear transition of a frictional crack propagating along an heterogeneous interface\n",
    "\n",
    "The main goal of this tutorial is to model the supershear transition of a mode II crack, and is inspired by the following paper : [Barras & al.](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.119.144101)\n",
    "\n",
    "We are considering two elastic semi-infinite body as shown in the following picture. The bodies are in contact at the position $y=0$. They are driven by a constant far-field load $\\tau_0$. An initial crack is present in the system of size $a_0$ (on the left of the interface). Its size is large enough that it starts to propagate in an unstable fashion. This crack will grow towards the right (Note that the growth is prevented in this problem in the left direction to reduce the study to a single-side propagation). The crack propagates first in a homogeneous field of toughness $G_c$ (in green in the figure). Then, it will propagate in alternate asperities with respectively higher / lower fracture toughness (orange and brown). The spatial average fracture toughness of the heterogeneous part of the interface is equal to $G_c$, such that the macroscopic toughness is the same along the interface. When propagating, the crack is expected to accelerate towards the limiting velocity (the Rayleigh wave speed $c_r$ for a mode II crack).\n",
    "\n",
    "<img src=\"model.png\" width=\"1000\">\n",
    "\n",
    "For a homogeneous interface, if the so-called seismic ratio is larger than a given value, the rupture will transition to super-shear velocities through the formation of a daughter crack ahead of the main rupture front. We consider here a case that should not involve super-shear transition. Nonetheless, the presence of toughness heterogeneities causes a supershear transition in this case.\n",
    "\n",
    "## Import the necessary modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# import cracklet\n",
    "import cracklet as cra\n",
    "# import numpy for vector manipulation\n",
    "import numpy as np\n",
    "# import the pyplot submodule to draw results\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Defining the geometry and the material parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nb_el_x = 2048 # Number of discretization points along x\n",
    "dom_size_x = 3 # Length of the domain\n",
    "nb_time_steps = 15000 # Number of time steps\n",
    "\n",
    "dx = dom_size_x / nb_el_x\n",
    "\n",
    "E = 5.3e9 # Young Modulus\n",
    "nu = 0.35 # Poisson ratio\n",
    "cs = 1263 # Shear wave speed\n",
    "tcut = 100 # Cut-off of the material kernel\n",
    "\n",
    "# Loading\n",
    "load = 22e5\n",
    "psi = 90\n",
    "phi = 0\n",
    "\n",
    "# Read input file to get some parameters\n",
    "\n",
    "cra.DataRegister.readInputFile(\"input_supershear.cra\")\n",
    "\n",
    "# Extract parameters from input file\n",
    "crt_op = cra.DataRegister.getParameterReal(\"critical_shear_opening\")\n",
    "max_str = cra.DataRegister.getParameterReal(\"max_shear_strength\")\n",
    "\n",
    "# Compute Griffith length\n",
    "G_length = crt_op * max_str / (load*load*np.pi) * E / (1-nu*nu)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating the model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create the model object\n",
    "model = cra.SpectralModel(nb_el_x,nb_time_steps,dom_size_x,nu,E,cs,tcut,\"Supershear\",\"./\")\n",
    "# Create a SimulationDriver object to help driving the simulation\n",
    "driver = cra.SimulationDriver(model)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating the heterogeneous interface"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create the interfacer\n",
    "interfacer = cra.InterfacerLinearCoupledCohesive(model)\n",
    "interfacer.createUniformInterface()\n",
    "# Create a small notch \n",
    "interfacer.createThroughCrack(0.,5*dx)\n",
    "# Create a succession of weak and strong patches\n",
    "start_het = 2*G_length\n",
    "end_het = 2\n",
    "nb_heterog = 50\n",
    "var_strength = 2/3\n",
    "var_opening = 0.\n",
    "nb_het = 40\n",
    "## var_strength and var_opening are repeated twice as one can change indepandently the normal and the shear component of the cohesive law\n",
    "x_end = interfacer.createThroughMultiPolAsperity(start_het,end_het,nb_het,var_strength,var_strength,var_opening,var_opening,False)\n",
    "\n",
    "wall_start = 0.9*dom_size_x\n",
    "wall_end = dom_size_x\n",
    "interfacer.createThroughWall(wall_start,wall_end)\n",
    "\n",
    "# Define the end of the propagation domain\n",
    "end_prop = 1.75"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Init the loading"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "driver.initConstantLoading(load,psi,phi)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Set-up the dumper"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dumper = cra.DataDumper(model)\n",
    "dumper.initDumper(\"STD_shear_vel.cra\",cra.DataFields._shear_velocity_jumps)\n",
    "dumper.initVectorDumper(\"STD_shear_tra.cra\",cra.DataFields._interface_tractions,0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Artificially grow the crack from zero length to its unstable size\n",
    "\n",
    "In the simulation, we start from a crack of a given size and artificially grow the crack by setting the resistance of the point next to the crack equal to 0. When the crack reaches its unstable size, it will start propagating spontaneously. This procedure allows us to ensure that we do not have a large burst in the system due to the introduction of a large crack instantaneously."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "min_vel = 0.2 # The crack will be artificially nucleated until it reaches a velocity of 0.2 c_s\n",
    "driver.launchCrack(0.,G_length,min_vel)\n",
    "print(\"The crack has been launched\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x_tip = cra.DataRegister.getCrackTipPosition(0,nb_el_x)*dx # Position of the tip\n",
    "t = 0\n",
    "\n",
    "while(x_tip<end_prop):\n",
    "    driver.solveStep()\n",
    "    \n",
    "    if (t%5==0):\n",
    "        dumper.dumpAll()\n",
    "\n",
    "    if (t%1000==0):\n",
    "        print(\"Crack position at {:.3f} %\".format(x_tip/dom_size_x*100))\n",
    "        \n",
    "    x_tip = cra.DataRegister.getCrackTipPosition(0,nb_el_x)*dx\n",
    "    t += 1\n",
    "    \n",
    "print(\"Simulation has reached the end\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualize the results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "timer = np.loadtxt(\"Timer_STD_shear_vel.cra\")\n",
    "timer = timer - timer[0]\n",
    "dt = timer[1] - timer[0]\n",
    "\n",
    "vel = np.loadtxt(\"STD_shear_vel.cra\")\n",
    "stress = np.loadtxt(\"STD_shear_tra.cra\")\n",
    "\n",
    "py,px = np.mgrid[0:timer[-1]:dt,0:dom_size_x:dx]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig,axe = plt.subplots(nrows=1,ncols=1,figsize=(10,5),dpi=100)\n",
    "axe.set_ylabel(r\"$t$ [s]\",fontsize=15)\n",
    "axe.set_xlabel(r\"$x$ [m]\",fontsize=15)\n",
    "axe.set_xlim([0,end_het])\n",
    "mesh = axe.pcolormesh(px,py,vel,vmin=0,vmax=20,shading=\"gouraud\")\n",
    "cbar = plt.colorbar(mesh)\n",
    "cbar.set_label(r\"$v_x$ [m/s]\",fontsize=15)\n",
    "\n",
    "fig.tight_layout()\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig,axe = plt.subplots(nrows=1,ncols=1,figsize=(10,5),dpi=100)\n",
    "axe.set_ylabel(r\"$t$ [s]\",fontsize=15)\n",
    "axe.set_xlabel(r\"$x$ [m]\",fontsize=15)\n",
    "axe.set_xlim([0,end_het])\n",
    "mesh = axe.pcolormesh(px,py,stress,vmin=0,vmax=7e6,shading=\"gouraud\")\n",
    "cbar = plt.colorbar(mesh)\n",
    "cbar.set_label(r\"$\\tau_{xy}$ [Pa]\",fontsize=15)\n",
    "\n",
    "fig.tight_layout()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
