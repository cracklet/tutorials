# Tutorial dependencies

In order to run these tutorials, you have to install the dependencies:

```sh
pip install -r requirements.txt
```

# cRacklet tutorials repository on Binder

## [Supershear transition along heterogeneous interface](https://mybinder.org/v2/gl/cracklet%2Ftutorials/72d3a17295b091c74cd57fc5647e9e91dcfc5e51?filepath=supershear%2Fsupershear.ipynb)

## [Frictional crack nucleation and propagation for rate and state friction](https://mybinder.org/v2/gl/cracklet%2Ftutorials/2cbe12707784f451c14ffe401e95ab5d6b415b8b?filepath=rate-and-state%2Frate_and_state.ipynb)
