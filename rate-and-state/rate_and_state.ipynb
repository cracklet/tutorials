{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# cRacklet Tutorial - Nucleation and propagation of a frictional crack on an interface describe by a rate and state friction law.\n",
    "\n",
    "The main goal of this tutorial is to model the nucleation and propagation of a frictional crack on an interface subjected to constant shear stress. The behavior of the interface is here described using a rate and state friction law. The simulations are similar to the ones conducted in the following paper : [Barras & al.](https://journals.aps.org/prx/abstract/10.1103/PhysRevX.9.041043)\n",
    "\n",
    "We are considering two elastic semi-infinite body as shown in the following picture. The bodies are in contact at the position $y=0$. They are driven by a constant out of plane far-field shear load $\\tau_0$ and a constant normal stress $\\sigma_0$. The entire interface is initially sliding at a steady-state velocity $v_0$.\n",
    "\n",
    "A perturbation in the state field is introduced at the center. Its size is large enough that this perturbation will grow and nucleate a frictional crack that will propagate dynamically.\n",
    "\n",
    "<img src=\"model_rate_and_state.png\" width=\"600\">\n",
    "\n",
    "## The friction law\n",
    "\n",
    "In the following figure we show the friction law in green as well as the initial condition of the system in brown, which corresponds to the interesction between the driving stress in dashed yellow and the friction law.\n",
    "\n",
    "<img src=\"friction_law.png\" width=\"600\">\n",
    "\n",
    "\n",
    "## Import the necessary modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# import cracklet\n",
    "import cracklet as cra\n",
    "# import numpy for vector manipulation\n",
    "import numpy as np\n",
    "# import matplotlib\n",
    "import matplotlib as mpl\n",
    "# import the pyplot submodule to draw results\n",
    "import matplotlib.pyplot as plt\n",
    "# import the color submodule to get cmap\n",
    "import matplotlib.cm as cm"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nb_el_x = 1024 # Number of discretization points along x\n",
    "dom_size_x = 20 # Length of the domain\n",
    "nb_time_steps = 4000 # Number of time steps\n",
    "\n",
    "dx = dom_size_x / nb_el_x\n",
    "\n",
    "mu = 9e9 # Shear modulus\n",
    "nu = 0.33 # Poisson ratio\n",
    "E = 2*mu*(1+nu) # Young Modulus\n",
    "cs = 2738.6 # Shear wave speed\n",
    "tcut = 100 # Cut-off of the material kernel\n",
    "\n",
    "# Loading\n",
    "load = 0.36e6\n",
    "psi = 90\n",
    "phi = 90\n",
    "\n",
    "# Read input file to get some parameters\n",
    "\n",
    "cra.DataRegister.readInputFile(\"input_rate_and_state.cra\")\n",
    "\n",
    "# Extract parameters from input file\n",
    "D = cra.DataRegister.getParameterReal(\"D_hom\")\n",
    "\n",
    "# Standard deviation for the perturbation\n",
    "std_dev = nb_el_x / 10\n",
    "# Maximum velocity of the perturbation\n",
    "vm = 0.01"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating the model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create the model object\n",
    "model = cra.SpectralModel(nb_el_x,nb_time_steps,dom_size_x,nu,E,cs,tcut,\"rate-and-state\",\"./\")\n",
    "# Init the model\n",
    "model.initModel(0.1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "v0 = cra.DataRegister.getParameterReal(\"v0\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating the rate and state interface"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create the interfacer\n",
    "interfacer = cra.InterfacerRegularizedRateAndState(model)\n",
    "r_and_s = model.getInterfaceLaw()\n",
    "r_and_s.initRegularizedStateEvolution(v0)\n",
    "interfacer.createUniformInterface()\n",
    "\n",
    "r_and_s.setVelocityPredictor([0.,0.,v0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Set the loading"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.setLoadingCase(load,psi,phi)\n",
    "model.updateLoads()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Set-up the dumper"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dumper = cra.DataDumper(model)\n",
    "dumper.initDumper(\"STD_state_variable.cra\", cra.DataFields._state_variable)\n",
    "dumper.initVectorDumper(\"STD_top_z_vel.cra\",cra.DataFields._top_velocities,2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Solve the problem"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "freq = 400\n",
    "\n",
    "t = 0\n",
    "while(t<nb_time_steps):\n",
    "    \n",
    "    model.updateDisplacements()\n",
    "    model.fftOnDisplacements()\n",
    "    model.computeStress()\n",
    "    \n",
    "    if(t==0):\n",
    "        model.initInterfaceFields()\n",
    "    else:\n",
    "        model.computeInterfaceFields()\n",
    "        \n",
    "    model.increaseTimeStep()\n",
    "        \n",
    "    if(t==5):\n",
    "        r_and_s.insertGaussianPerturbation(std_dev,vm)\n",
    "\n",
    "    if ((t-5)%freq==0):\n",
    "        print(\"Simulation is at {:.0f}%\".format(t/nb_time_steps*100))\n",
    "        dumper.dumpAll()\n",
    "    t += 1\n",
    "    \n",
    "print(\"Simulation has reached the end\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualize the results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "timer = np.loadtxt(\"Timer_STD_top_z_vel.cra\")\n",
    "timer = timer - timer[0]\n",
    "dt = timer[1] - timer[0]\n",
    "\n",
    "top_z_vel = np.loadtxt(\"STD_top_z_vel.cra\")\n",
    "state = np.loadtxt(\"STD_state_variable.cra\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### Evolution of the slip velocity\n",
    "\n",
    "x = np.arange(nb_el_x)/nb_el_x*dom_size_x\n",
    "fig,axe1 = plt.subplots(nrows=1,ncols=1,figsize=(8,4),dpi=150)\n",
    "axe1.set_ylabel(\"Relative slip velocity \\n $ 2 v^{top}$ [m/s]\",fontsize=15)\n",
    "axe1.set_xlabel(\"Position $x$ [m]\",fontsize=15)\n",
    "\n",
    "nb_outputs = len(timer)\n",
    "\n",
    "my_cmap = cm.get_cmap(\"seismic\")\n",
    "norm = mpl.colors.Normalize(vmin=0, vmax=timer[-1])\n",
    "# creating ScalarMappable\n",
    "sm = plt.cm.ScalarMappable(cmap=my_cmap, norm=plt.Normalize(vmin=0, vmax=timer[-1]))\n",
    "\n",
    "for i in range(nb_outputs):\n",
    "    line = axe1.plot(x,top_z_vel[i],linewidth=2,color=my_cmap(norm(timer[i])))\n",
    "\n",
    "cbar = plt.colorbar(sm)\n",
    "cbar.ax.set_title(r\"$t$ [s]\")\n",
    "\n",
    "axe1.set_xlim([0,dom_size_x])\n",
    "fig.tight_layout()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
